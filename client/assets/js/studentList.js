let adminUser = localStorage.getItem("isAdmin")
//retrieve the JWT stored in our local storage
let token = localStorage.getItem("token");
//retrieve the userId stored in our local storage
let userId = localStorage.getItem("id");
let profileContainer = document.querySelector("#profileContainer")
let courseContainer = document.querySelector("#courseContainer")
let userContainer = document.querySelector("#userContainer")

console.log(userId)
fetch('http://localhost:3000/api/users')
.then(res => res.json())
.then( data => {
    for(let i = 0; i < data.length; i++){
        let user = []
        let fullName = []
        let courseID = []
        let courseName = []
        user.push(data[i].firstName + " " + data[i].lastName)
        console.log(user)
        fullName.push(data[i].firstName + " " + data[i].lastName)
        let enrollments = data[i].enrollments
        for(let i = 0; i < enrollments.length; i++){
            courseID.push(enrollments[i].courseId)
            fetch('http://localhost:3000/api/courses')
            .then(res => res.json())
            .then(data => {
                for(let i = 0; i < data.length; i++){
                    if(courseID.includes(data[i]._id)){
                        courseName.push(user + " " + data[i].name)

                    }
                }
            })
           
        }
     
        userContainer.innerHTML +=
        `	<div class="col-12 px-1 mt-2" >
        <div class="card p-2 px-3 py-3 shadow bg-secondary text-white" id="card-course">
            <h4 class="text-center pt-3">${fullName}</h4>
            <div class="card-body bg-light text-muted shadow" id="descriptionBody">
            ${courseID}
            </div>
        </div>
    </div>`

    }
})
